\documentclass[10pt,conference,compsocconf]{IEEEtran}

\usepackage{hyperref}
\usepackage{graphicx}

\usepackage{amsmath}
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)}

\usepackage{amsfonts}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\G}{\mathbb{G}}

\begin{document}
\title{Privacy Preserving but Analysable Encryption Schemes}

\author{
  Cedric Maire \\
  \emph{School of Computer and Communication Sciences, EPF Lausanne, Switzerland}
}

\maketitle

% Cite: ~\cite{tag_name0, tag_name1, ...}

% \section{section_name}
% \section*{Acknowledgements} NON-COUNTED TITLE
% \subsection{subsection_name}
% \subsubsection{Installation}
% \label{sec:section_name}
% ~\ref{sec:section_name}

\begin{abstract}
Data privacy becomes increasingly important due to the massive amount of data that is generated everyday. At the same time, outsourced cloud computing becomes popular among many industries due to its effectiveness. The main problem that arises is the fact that data analytics and mining is mostly impossible on encrypted data: any modification will make the ciphertext invalid and undecipherable. In addition, randomness makes comparison impossible. This means that in order to extract useful information, analysis has to be done in the plaintext domain. Sectors such as medical research cannot profit  of outsourced cloud computing without being worried about the privacy of the data. New encryption schemes allowing data processing in the ciphertext domain are being researched. In short, these encryption schemes enable complex processing of data without the need of decryption and thus preserve the privacy and secrecy.
\end{abstract}

\section{Privacy Protected Data Analytics and Mining}

Data analytics and mining requires processing of the data, which is mostly only feasible in plaintext form. \emph{Privacy protected data analytics and mining} is a research area developing new encryption schemes allowing data processing in the ciphertext domain without revealing the original messages. There are mainly three encryption scheme types with their own properties: \emph{order preserving encryption}, \emph{searchable encryption} and \emph{homomorphic encryption}. In addition to that comes \emph{data masking} which consists of hiding or obfuscating data and isn't an encryption scheme. What follows are explanations of the three latter cryptographic systems:

\begin{description}
\item[Order Preserving Encryption] \ \\
Preserves the original ordering of the plaintexts.
\item[Searchable Encryption] \ \\
Allows the ciphertexts to be securely searched for keywords. These keywords are not revealed and no other information than the fact that the ciphertext contains one of them can be extracted.
\item[Homomorphic Encryption] \ \\
Allows operations on ciphertexts without making it undecipherable. In addition, these operations are visible in the plaintext domain if the newly created ciphertexts are decrypted.
\end{description}

\section{Order Preserving Encryption}

As said before, the main property of order preserving encryption is that the generated ciphertexts keep the original ordering of the plaintexts. Written formally this means that, given two plaintexts $pt_{0}$ and $pt_{1}$ and an encryption function $Enc(\cdot)$, the function $Enc(\cdot)$ maintains the ordering of $pt_{0}$ and $pt_{1}$. Mathematically this gives: $pt_{0} < pt_{1} \Rightarrow ct_{0} < ct_{1}$, where $ct_{i} = Enc(pt_{i})$ and $<$ compares the numerical orders ~\cite{10.1007/978-3-662-45608-8_3}. This property is interesting especially in database systems where functions like MAX, MIN, COUNT or ORDER BY are heavily used. By using an order preserving encryption scheme these comparisons can be applied directly on the encrypted data without need of decryption ~\cite{wiki:order_preserving_encryption}. This type of encryption schemes is somewhat \emph{leaky}, this means that they'll give some information about two plaintexts, given two ciphertexts. Implied by the definition above, order preserving encryption schemes have to give some information about the plaintext distance. The distance $ct_{i} - ct_{j}$ has to proportionally increase along with $pt_{i} - pt_{j}$ ~\cite{10.1007/978-3-662-45608-8_3}. This property might be unwanted in some cases but is highly valuable in others where it will be possible to create indexes on these ciphertexts.

\section{Searchable Encryption}

Consider a setting in which a user $A$ wants to redirect emails to different devices depending on some keyword they contain. Let's suppose that emails and all of their metadata are fully encrypted using a public key encryption scheme. In order to not violate the privacy of user $A$, the server needs to process this encrypted data and deterministically decide where to route the emails based on these keywords. The requirements are that the server should not be able to know what these keywords are and also not be able to extract any other information than the fact that it contains one or more specific \emph{unknown} keywords ~\cite{10.1007/978-3-540-24676-3_30}. \\
Given a plaintext $pt$, keywords ${\{w_i\}}_{i = 0}^{n - 1}$, a standard public key encryption system with function $Enc_{A_{pub}}(\cdot)$ and a public key encryption \emph{with keyword search} system with function $EncKS_{A_{pub}}(\cdot)$, where $A_{pub}$ is the public key of user $A$, one can produce a ciphertext $ct = Enc_{A_{pub}}(pt) \ \| \ EncKS_{A_{pub}}(w_{0}) \ \| \ ... \ \| \ EncKS_{A_{pub}}(w_{n - 1})$, where $\|$ denotes concatenation. The main point of such an encryption scheme is to enable anyone, without the need of the secret key, to test wether $ct$ contains a certain keyword. To do so, user $A$ provides a \emph{trapdoor} $T_{W}$. Given $EncKS_{A_{pub}}(W')$ and $T_{W}$, anyone should be able to test wether $W = W'$ without the knowledge of $W$ nor $W'$. The trapdoor $T_{W}$ is created using $A_{sec}$, the secret key of user $A$.~\cite{10.1007/978-3-540-24676-3_30}

\section{Homomorphic Encryption}

Given plaintexts $pt_{0}, \ pt_{1}, \ ..., \ pt_{n - 1}$, corresponding ciphertexts $ct_{0}, \ ct_{1}, \ ..., \ ct_{n - 1}$, an encryption function $Enc(\cdot)$ and two functions $f(\cdot, \ ...)$ and $g(\cdot, \ ...)$ over the plaintext and ciphertext domains, respectively, anyone (and not only the key-holder) should be able to process a ciphertext $ct_{i} = Enc(pt_{i})$ to create a new ciphertext $ct_{i}' = g(ct_{i}, \ ...)$ that is a valid encryption. This means that $ct_{i}' = g(ct_{i}, \ ...) = Enc(pt_{i}') = Enc(f(pt_{i}, \ ...))$, where $pt_{i}' = f(pt_{i}, \ ...)$. All these computations are performed without the need of the secret key and no information about the plaintext should leak at any time~\cite{homenc}. Homomorphic encryption schemes are by definition \emph{malleable}. \\
There exists two types of homomorphic encryption schemes: \textit{partially homomorphic} and \textit{fully homomorphic}~\cite{wiki:Homomorphic_encryption}. Partially means that the types of operation are limited (for example only additions are allowed and multiplications are forbidden) or that the number of these operations is limited to some upper bound (for example at most a single multiplication is allowed). Unpadded RSA and ElGamal, two well known encryption schemes, are partially homomorphic. Here is why:

\begin{description}
\item[Unpadded RSA] \ \\
Given RSA modulus $m$ and encryption exponent $e$ the ciphertext $ct$ of the plaintext $pt$ is computed as follows: $ct = Enc(pt) = pt^{e} \Mod{m}$. Given two ciphertexts $ct_{0}$ and $ct_{1}$ one can compute $ct_{0}ct_{1} = (pt_{0}^{e} \Mod{m})(pt_{1}^{e} \Mod{m}) = (pt_{0} \cdot pt_{1})^{e} \Mod{m} = Enc(pt_{0} \cdot pt_{1})$.
\item[ElGamal] \ \\
Given a cyclic group $\G$ of order $q$, a generator $g$ and a public key $h = g^x$, where $x$ is the secret key, the encryption of a plaintext $pt$ is $ct = Enc(pt) = (g^r, pt \cdot h^r)$ for some random $r \in \Z_{q}$. Given two ciphertexts $ct_{0}$ and $ct_{1}$ one can compute $ct_{0} \cdot ct_{1} = (g^{r_{0}}, pt_{0} \cdot h^{r_{0}})(g^{r_{1}}, pt_{1} \cdot h^{r_{1}}) = (g^{r_{0} + r_{1}}, pt_{0} \cdot pt_{1} \cdot h^{r_{0} + r_{1}}) = Enc(pt_{0} \cdot pt_{1})$.
\end{description}

Craig Gentry proposed in his Ph.D. thesis the first fully homomorphic encryption scheme that was viable ~\cite{homenc}. Even though his thesis seems very interesting, the full reading and analysis of his system is beyond the scope of this report. For the sake of completeness: most of the fully homomorphic cryptographic systems rely on the \emph{learning with errors problem} and Craig Gentry showed how to design a fully homomorphic encryption scheme starting from a partially homomorphic encryption scheme ~\cite{Regev10, homenc}.

\section{Data Masking for Analytics}

Data masking, in opposite to the three latter presented systems, is not an encryption scheme. This means that the main purpose of data masking is not to encrypt but to replace or mask data that is classified identifiable or sensitive with some other realistic values in order to be able to analyse production databases without being able to assign data to a person or showing private information such as credit card numbers ~\cite{data_masking_best_practice_white_paper_2018}. The difficulty is to replace it with data that looks real, stays consistent and does not break the underlying analysis. There exists different techniques that can be applied such as: substitution, shuffling, deletion and even encryption ~\cite{wiki:Data_masking}.

\section{Summary}

Privacy protected data analytics and mining is still at the very beginning of its research. Some usable encryption schemes have already been developed but the lack of standardization makes it difficult to implement them in production. Especially homomorphic encryption schemes, which require to be fully processable, are difficult to design without leaking information about the plaintexts or producing an undecipherable ciphertext and, at the same time, be efficient in computation, thus usable in practice ~\cite{Wu15}. To end this discussion, homomorphic encryption schemes allow for the most flexible processing among all three system types since they have to satisfy their properties for any valid functions $f(\cdot, \ ...)$ and $g(\cdot, \ ...)$.

\bibliographystyle{IEEEtran}
\bibliography{literature}

\end{document}
